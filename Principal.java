import saludo.Saludo;
public class Principal {
    public static void main(String[] args) {
        //Tarea del tp1
        System.out.println("Hola " + args[0] + " bienvenido al mundo java");
        System.out.println("¡Mucha suerte!\n");
        
        //Actividad de la clase practica
        Saludo saludo1 = new Saludo();
        Saludo saludo2 = new Saludo();
        saludo1.setSaludoInicial("Hola");
        saludo1.SaludoDeMarugada();
        saludo2.SaludoDeTarde();
        saludo1.SaludoDeNoche();

        System.out.println("\nEstado del objeto: " + saludo1.getSaludoInicial());

        
   
    }
    
}