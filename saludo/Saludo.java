package saludo;

public class Saludo {
    
    private String saludoInicial;

    public void setSaludoInicial(String saludoInicial){
        this.saludoInicial = saludoInicial;
    }

    public String getSaludoInicial(){
        return saludoInicial;
    }
    
    public void SaludoDeMarugada(){
        System.out.println("Hola, Buenos dias");
    }
    
    public void SaludoDeTarde(){
        System.out.println("Hola, Buenos Tardes");
    }
    
    public void SaludoDeNoche(){
        System.out.println("Buenas Noches");
    }
}
